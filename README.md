## register-cenhomes
register-cenhomes

__URL:__ `https://sso.cenhomes.vn/g-sso-gateway/v2/auth/users/register-cenhomes`

__Method:__ `POST`

__Auth required:__ `NO`

__body__: `[{\"username\":\"namng\",\"password\":\"123456\",\"mobile\":\"123456\",\"email\":\"123456\",\"fullname\":\"123456\"}]`

type: enum


__Header:__ auth

Key | Value
--- | ---
Content-type | application/json

__Request example:__

```bash
curl -X POST "https://sso.cenhomes.vn/g-sso-gateway/v2/auth/users/register-cenhomes" -H  "accept: application/json" -H  "Content-Type: application/json" -d "[{\"username\":\"namng\",\"password\":\"123456\",\"mobile\":\"123456\",\"email\":\"123456\",\"fullname\":\"123456\"}]"
```

__Response example:__

```json
{
"status": 0,
"payload": {
"data": {}
},
"description": ""
}
```

## account-cenhomes
account-cenhomes

__URL:__ `https://sso.cenhomes.vn/g-sso-gateway/v2/auth/account-cenhomes`

__Method:__ `POST`

__Auth required:__ `NO`

__body__: `{\"username\":\"namng\",\"password\":\"123456\",\"mobile\":\"123456\",\"email\":\"123456\",\"IdEncode\":\"123456\"}`

type: enum


__Header:__ auth

Key | Value
--- | ---
Content-type | application/json

__Request example:__

```bash
curl -X POST "https://sso.cenhomes.vn/g-sso-gateway/v2/auth/account" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"username\":\"namng\",\"password\":\"123456\",\"mobile\":\"123456\",\"email\":\"123456\",\"IdEncode\":\"123456\"}
```

__Response example:__

```json
{
"status": 0,
"payload": {
"data": {}
},
"description": ""
}
```
